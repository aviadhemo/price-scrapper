const puppeteer = require('puppeteer');

let scrap = async (url) => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    
    await page.goto(url);
    let result = null;

    try {
        result = await page.evaluate(() => {
            const title = document.querySelector('h1').innerText;
            const productName = document.querySelector('#breadcrumbsContainer > div > span:nth-child(4) > span').innerText;
            const imageUrl = document.querySelector('#div_MainImage > img').src;
            const minPrice = document.querySelector('#sectionContent > div.InfoBox > div.Prices > div:nth-child(1) > div.PricesTxt > span:nth-child(2)').innerText;
            const maxPrice = document.querySelector('#sectionContent > div.InfoBox > div.Prices > div:nth-child(1) > div.PricesTxt > span:nth-child(1)').innerText;
            
            return {
                title,
                url: document.location.href,
                productName,
                imageUrl,
                minPrice,
                maxPrice
            };
        });
    } catch(e) {
        console.log(e);
    } finally {
        browser.close();
    }
    
    return result;
};

// scrap('http://www.zap.co.il/model.aspx?modelid=962178').then((value) => {
//     console.log(JSON.stringify(value, null, 2));
// });
module.exports = {
    scrap
};
